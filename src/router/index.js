import Vue from "vue";
import Router from "vue-router";
import searchMovie from "@/pages/searchMovie";

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: "/",
      component: searchMovie
    }
  ]
});
